import React from "react";
import { Location } from "@prisma/client";

type HeaderProps = {
  restaurantName: string;
  restaurantLocation: Location;
};

export default function Header({
  restaurantName,
  restaurantLocation,
}: HeaderProps) {
  return (
    <div className="h-96 overflow-hidden">
      <div className="flex h-full items-center justify-center bg-gradient-to-r from-[#0f1f47] to-[#5f6984] bg-center">
        <h1 className="text-center text-7xl capitalize text-white">
          {restaurantName} ({restaurantLocation.name})
        </h1>
      </div>
    </div>
  );
}
