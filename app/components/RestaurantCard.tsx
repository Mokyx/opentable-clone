import React from "react";
import Link from "next/link";
import RestaurantPrice from "@/app/components/RestaurantPrice";
import { Prisma, PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();
export const RestaurantCardSelect = {
  id: true,
  name: true,
  main_image: true,
  cuisine: true,
  price: true,
  slug: true,
} satisfies Prisma.RestaurantSelect;

export type RestaurantCardType = Prisma.RestaurantGetPayload<{
  select: typeof RestaurantCardSelect;
}>;

export const fetchRestaurants = async () => {
  return prisma.restaurant.findMany({
    select: RestaurantCardSelect,
  });
};

type RestaurantCardProps = {
  restaurant: RestaurantCardType;
};

export default function RestaurantCard({ restaurant }: RestaurantCardProps) {
  return (
    <div className="m-3 h-72 w-64 cursor-pointer overflow-hidden rounded border">
      <Link href={`/restaurant/${restaurant.slug}`}>
        <img src={restaurant.main_image} alt="" className="h-36 w-full" />
        <div className="p-2">
          <h3 className="mb-2 text-2xl font-bold">{restaurant.name}</h3>
          <div className="flex items-start">
            <div className="mb-2 flex">*****</div>
            <p className="ml-2">77 reviews</p>
          </div>
          <div className="flex text-reg font-light capitalize">
            <p className="mr-3">{restaurant.cuisine?.name}</p>
            <p className="mr-3">
              <RestaurantPrice price={restaurant.price} />
            </p>
            <p className="mr-3">Rennes</p>
          </div>
          <p className="mt-1 text-sm font-bold">Booked 3 times today</p>
        </div>
      </Link>
    </div>
  );
}
