import { PRICE } from "@prisma/client";
import React from "react";

export default function RestaurantPrice({
  price,
}: {
  price: PRICE;
}): React.JSX.Element {
  switch (price) {
    case PRICE.CHEAP:
      return (
        <>
          <span>$$</span>
          <span className="text-gray-300">$$</span>
        </>
      );
    case PRICE.REGULAR:
      return (
        <>
          <span>$$$</span>
          <span className="text-gray-300">$</span>
        </>
      );
    case PRICE.EXPENSIVE:
      return <span>$$$$</span>;
    default:
  }
  return <div />;
}
