import RestaurantCard, {
  fetchRestaurants,
} from "@/app/components/RestaurantCard";
import SearchBar from "@/app/search/components/SearchBar";

export default async function Homepage() {
  const restaurants = await fetchRestaurants();
  return (
    <>
      <div className="h-64 bg-gradient-to-r from-[#0f1f47] to-[#5f6984] p-2">
        <div className="mt-10 text-center">
          <h1 className="text-5xl font-bold text-gray-100">
            Find your Table for any occasion
          </h1>
        </div>
        <SearchBar />
      </div>
      <div className="mt-10 flex flex-wrap px-36 py-3">
        {restaurants.map((restaurant) => (
          <RestaurantCard restaurant={restaurant} />
        ))}
      </div>
    </>
  );
}
