import React from "react";

export default function Reserve() {
  return (
    <div className="m-auto w-3/5 py-9">
      <div>
        <h3 className="font-bold">You&apos;re almost Done!</h3>
        <div className="mt-5 flex">
          <img
            src="https://resizer.otstatic.com/v2/photos/wide-huge/2/50154242.webp"
            alt=""
            className="h-16 w-32 rounded"
          />
          <div className="ml-4">
            <h1 className="text-3xl font-bold">Le Falafel</h1>
            <div className="mt-3 flex">
              <p className="mr-6">Tues, 22, 2023</p>
              <p className="mr-6">7:30 PM</p>
              <p className="mr-6">3 people</p>
            </div>
          </div>
        </div>
      </div>
      <div className="mt-10 flex w-[660px] flex-wrap justify-between">
        <input
          type="text"
          className="mb-4 w-80 rounded border p-3"
          placeholder="First Name"
        />
        <input
          type="text"
          className="mb-4 w-80 rounded border p-3"
          placeholder="Last Name"
        />
        <input
          type="text"
          className="mb-4 w-80 rounded border p-3"
          placeholder="Phone Number"
        />
        <input
          type="text"
          className="mb-4 w-80 rounded border p-3"
          placeholder="Email"
        />
        <input
          type="text"
          className="mb-4 w-80 rounded border p-3"
          placeholder="Occasion (optionnal)"
        />
        <input
          type="text"
          className="mb-4 w-80 rounded border p-3"
          placeholder="Requests (optionnal)"
        />
        <button
          type="button"
          className="w-full rounded bg-red-600 p-3 font-bold text-white disabled:bg-gray-300"
        >
          Complete Reservation
        </button>
      </div>
    </div>
  );
}
