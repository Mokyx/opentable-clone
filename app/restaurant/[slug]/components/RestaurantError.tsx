import React from "react";

export default function RestaurantError() {
  return (
    <div className="h-6 text-red-600">
      Error : Unable to fetch restaurant data
    </div>
  );
}
