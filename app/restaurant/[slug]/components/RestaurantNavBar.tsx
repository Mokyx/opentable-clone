import Link from "next/link";
import React from "react";

export default function RestaurantNavBar({
  restaurantSlug,
}: {
  restaurantSlug: string;
}) {
  return (
    <nav className="flex border-b pb-2 text-reg">
      <Link className="mr-7" href={`/restaurant/${restaurantSlug}`}>
        Overview
      </Link>
      <Link href={`/restaurant/${restaurantSlug}/menu`}>Menu</Link>
    </nav>
  );
}
