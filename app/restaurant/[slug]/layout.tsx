import Header from "@/app/components/Header";
import React from "react";
import { getRestaurant } from "@/app/restaurant/[slug]/utils/getRestaurant";
import RestaurantError from "@/app/restaurant/[slug]/components/RestaurantError";

type RestaurantLayoutProps = {
  children: React.ReactNode;
  params: { slug: string };
};

export default async function RestaurantLayout({
  children,
  params,
}: RestaurantLayoutProps) {
  const restaurant = await getRestaurant(params.slug);
  if (!restaurant) {
    return <RestaurantError />;
  }
  return (
    <>
      <Header
        restaurantName={restaurant.name}
        restaurantLocation={restaurant.location}
      />
      <div className="m-auto -mt-11 flex w-2/3 items-start justify-between">
        {children}
      </div>
    </>
  );
}
