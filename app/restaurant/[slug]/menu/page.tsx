import ReservationCard from "@/app/restaurant/[slug]/components/RervationCard";
import RestaurantError from "@/app/restaurant/[slug]/components/RestaurantError";
import RestaurantNavBar from "@/app/restaurant/[slug]/components/RestaurantNavBar";
import { getRestaurant } from "@/app/restaurant/[slug]/utils/getRestaurant";

export default async function RestaurantMenu({
  params,
}: {
  params: { slug: string };
}) {
  const restaurant = await getRestaurant(params.slug);
  if (!restaurant) {
    return <RestaurantError />;
  }
  return (
    <>
      <div className="w-[70%] rounded bg-white p-3 shadow">
        <RestaurantNavBar restaurantSlug={params.slug} />
        <main className="mt-5 bg-white">
          <div>
            <div className="mb-1 mt-4 pb-1">
              <h1 className="text-6xl font-bold">Menu</h1>
            </div>
            <div className="flex flex-wrap justify-between">
              <div className="mb-3 w-[49%] rounded border p-3">
                {restaurant.items.map((item) => {
                  return (
                    <>
                      <h3 className="text-lg font-bold">{item.name}</h3>
                      <p className="mt-1 text-sm font-light">
                        {item.description}
                      </p>
                      <p className="mb-7 mt-4">{item.price}</p>
                    </>
                  );
                })}
              </div>
            </div>
          </div>
        </main>
      </div>
      <ReservationCard />
    </>
  );
}
