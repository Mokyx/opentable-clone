import RestaurantNavBar from "@/app/restaurant/[slug]/components/RestaurantNavBar";

import ReservationCard from "@/app/restaurant/[slug]/components/RervationCard";
import RestaurantError from "@/app/restaurant/[slug]/components/RestaurantError";
import { getRestaurant } from "@/app/restaurant/[slug]/utils/getRestaurant";

export default async function Restaurant({
  params,
}: {
  params: { slug: string };
}) {
  const restaurant = await getRestaurant(params.slug);
  if (!restaurant) {
    return <RestaurantError />;
  }
  return (
    <>
      <div className="w-[70%] rounded bg-white p-3 shadow">
        <RestaurantNavBar restaurantSlug={params.slug} />
        <div className="mt-4 border-b pb-6">
          <h1 className="text-6xl font-bold">{restaurant.name}</h1>
        </div>
        <div className="flex items-end">
          <div className=" mt-2 flex items-center">
            <p>*****</p>
            <p className="ml-3 text-reg">4.9</p>
          </div>
          <div>
            <p className="ml-4 text-reg">600 Reviews</p>
          </div>
        </div>
        <div className="mt-4">
          <p className="text-lg font-light">{restaurant.description}</p>
        </div>
        <div>
          <h1 className="mb-7 mt-10 border-b pb-5 text-3xl font-bold">
            {restaurant.images.length} Photos
          </h1>
          <div className="flex flex-wrap">
            {restaurant.images.map((image) => (
              <img src={image} alt="" className="mb-1 mr-1 h-44 w-56" />
            ))}
          </div>
        </div>
        <div>
          <h1 className="mb-7 mt-10 border-b pb-5 text-3xl font-bold">
            What 100 people are saying
          </h1>
          <div>
            <div className="mb-7 border-b pb-7">
              <div className="flex">
                <div className="flex w-1/6 flex-col items-center">
                  <div className="mb-2 flex h-16 w-16 items-center justify-center rounded-full bg-blue-400">
                    <h2 className="text-lg text-white">MJ</h2>
                  </div>
                  <p className="text-center">Michael Jordan</p>
                </div>
                <div className="ml-10 w-5/6">
                  <div className="flex items-center">
                    <div className="mr-5 flex">*****</div>
                  </div>
                  <div className="mt-5">
                    <p className="text-lg font-light">
                      A family owned business with amazing food and service!
                      Highly recommended!
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ReservationCard />
    </>
  );
}
