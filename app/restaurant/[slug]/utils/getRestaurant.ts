import { Prisma, PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export const RestaurantSelect = {
  id: true,
  name: true,
  main_image: true,
  cuisine: true,
  price: true,
  items: true,
  slug: true,
  description: true,
  images: true,
  location: true,
} satisfies Prisma.RestaurantSelect;

export type RestaurantType = Prisma.RestaurantGetPayload<{
  select: typeof RestaurantSelect;
}> | null;

export const getRestaurant = async (slug: string) => {
  return prisma.restaurant.findUnique({
    where: { slug },
    select: RestaurantSelect,
  });
};
