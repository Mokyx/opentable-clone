"use client";

import React, { useState } from "react";
import { useRouter } from "next/navigation";

export default function SearchBar() {
  const router = useRouter();
  const [location, setLocation] = useState("");
  return (
    <div className="m-auto flex justify-center py-3 text-left">
      <input
        className="mr-3 w-[450px] rounded bg-white p-2 pl-3 text-lg"
        type="text"
        placeholder="State, city or town"
        value={location}
        onChange={(e) => {
          setLocation(e.target.value);
        }}
      />
      <button
        className="bg-red-600 px-9 py-2 text-white"
        type="button"
        onClick={() => {
          if (location === "banana") return;
          router.push("/search");
        }}
      >
        Lets go !
      </button>
    </div>
  );
}
