import React from "react";

export default function SearchCard() {
  return (
    <div className="flex border-b pb-5">
      <img
        src="https://resizer.otstatic.com/v2/photos/xlarge/1/50154230.webp"
        alt=""
        className="w-44 rounded"
      />
      <div className="pl-5">
        <h2 className="text-3xl">Aiana Restaurant Collective</h2>
        <div className="flex items-start">
          <div className="mb-2 flex">*****</div>
          <p className="ml-2 text-sm">Awesome</p>
        </div>
        <div className="mb-9">
          <div className="flex text-reg font-light">
            <p className="mr-4">$$$</p>
            <p className="mr-4">Mexican</p>
            <p className="mr-4">Ottawa</p>
          </div>
        </div>
        <div className="text-red-600">
          <a href="">View More Information</a>
        </div>
      </div>
    </div>
  );
}
