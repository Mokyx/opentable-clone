import React from "react";
import SearchBar from "@/app/search/components/SearchBar";
import SearchCard from "@/app/search/components/SearchCard";

export default function Search() {
  return (
    <>
      <div className="bg-gradient-to-r from-[#0f1f47] to-[#5F6984] p-2">
        <SearchBar />
      </div>
      <div className="m-auto flex w-2/3 items-start justify-between py-4">
        <div className="FILTERS w-1/5">
          <div className="border-b pb-4">
            <h1 className="mb-2 font-bold">Region</h1>
            <p className="text-reg font-light">Toronto</p>
            <p className="text-reg font-light">Ottawa</p>
            <p className="text-reg font-light">Montreal</p>
            <p className="text-reg font-light">Hamilton</p>
            <p className="text-reg font-light">Kingston</p>
            <p className="text-reg font-light">Niagara</p>
          </div>
          <div className="mt-3 border-b pb-4">
            <h1 className="mb-2 font-bold">Cuisine</h1>
            <p className="text-reg font-light">Mexican</p>
            <p className="text-reg font-light">Italian</p>
            <p className="text-reg font-light">Chinese</p>
          </div>
          <div className="mt-3 pb-4">
            <h1 className="mb-2 font-bold">Price</h1>
            <div className="flex">
              <button className="w-full rounded-l border-y border-l p-2 text-reg font-light">
                $
              </button>
              <button className="w-full border p-2 text-reg font-light">
                $$
              </button>
              <button className="w-full rounded-r border-y border-r p-2 text-reg font-light">
                $$$
              </button>
            </div>
          </div>
        </div>
        <div className="ml-6 w-5/6">
          <SearchCard />
        </div>
      </div>
    </>
  );
}
